app.service("atividadeService", function (requestFactory) {

  var findAll = function (success) {
    requestFactory.findAll("atividadeRest", success);
  }

  var findByExample = function (filter, success) {
    requestFactory.findByExample("atividadeRest", filter, success);
  }

  var insert = function (filter, success) {
    requestFactory.insert("atividadeRest", filter, success);
  }

  var update = function (filter, success, error) {
    requestFactory.update("atividadeRest", filter, success, error);
  }

  var remove = function (filter, success, error) {
    requestFactory.remove("atividadeRest", filter, success, error);
  }

  return {
    findAll: findAll,
    findByExample: findByExample,
    insert: insert,
    update: update,
    remove: remove
  }
});
