app.service("tipoAtividadeService", function (requestFactory) {
    var findAll = function (success) {
      requestFactory.findAll("tipoAtividadeRest", success);
    }

    return {
        findAll: findAll
    }
});
