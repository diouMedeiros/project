app.service("statusAtividadeService", function (requestFactory) {
    var findAll = function (success) {
      requestFactory.findAll("statusAtividadeRest", success);
    }

    return {
        findAll: findAll
    }
});
