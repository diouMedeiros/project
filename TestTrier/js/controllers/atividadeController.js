app.controller("atividadeController", function($rootScope, $scope, $route, $location, requestFactory, atividadeService, tipoAtividadeService, statusAtividadeService) {

    var _ABRIR = 1;
    var _CONCLUIR = 2;

    var active = function () {
        $scope.insert = $route.current ? ($route.current.$$route ? $route.current.$$route.inserting : false) : false;
		$scope.update = $route.current ? ($route.current.$$route ? $route.current.$$route.editing : false) : false;

        if ($scope.update) {
            $scope.atividade = $rootScope.object;
            $scope.aberta = $scope.atividade.statusAtividade.cdStatusAtividade === 1;
            $scope.tipoAtividadeList[0] = $scope.atividade.tipoAtividade;
        } else {
            loadCombos(function() {
                $scope.search();
            });
        }
    }

    var loadCombos = function (callback) {
        getTipoAtividadeList(function(response) {
            $scope.tipoAtividadeList = response.data;
            getStatusAtividadeList(function(response) {
                $scope.statusAtividadeList = response.data;
                $scope.statusAtividade = $scope.statusAtividadeList[0];
                callback();
            });
        });
    }

    var getTipoAtividadeList = function (callback) {
        tipoAtividadeService.findAll(function(response) {
            callback(response);
        });
    }


    var getStatusAtividadeList = function (callback) {
        statusAtividadeService.findAll(function(response) {
            callback(response);
        });
    }

    /** Método de busca **/
    $scope.search = function () {
        var filter = {};
        if ($scope.tipoAtividade) {
            filter.tipoAtividade = {cdTpAtividade:$scope.tipoAtividade.cdTpAtividade}
        }
        if ($scope.statusAtividade) {
            filter.statusAtividade = {cdStatusAtividade:$scope.statusAtividade.cdStatusAtividade}
        }
        console.log(filter);
        atividadeService.findByExample(filter, function(response) {
            $scope.atividadeList  = response.data;
            $scope.hasAtividades = $scope.atividadeList.length >0
        });
    }

    $scope.new = function () {
        $location.path("/cadAtividade");
    }

    $scope.edit = function (atividade) {
        $rootScope.object = atividade;
        $location.path("/editAtividade");
    }

    $scope.back = function () {
        $location.path("./listAtividade");
    }

    /** Mudança de status da atividade **/
    $scope.changeStatus = function (atividade) {
        if (!$scope.aberta || validateAtividades(atividade)) {
            atividade.statusAtividade.cdStatusAtividade = $scope.aberta ? _CONCLUIR : _ABRIR;

        } else {
            $scope.error = "Descrição de atividade com menos de 50 caracteres, modifique a descrição!";
            return
        }
        atividadeService.update(atividade, function() {
            $scope.back();
        });
    }

    /** insertOrUpdate **/
    $scope.saveAtividade = function(atividade, status) {
        if (!validateInsertOrUpdate(atividade)) {
            $scope.error = "Não é possível inserir manutenção urgente após as 13:00 hrs de setxa-feira!";
            return
        }
        if ($scope.insert) {
            atividadeService.insert(atividade, function() {
                $scope.back();
            });
        } else {
            atividadeService.update(atividade, function() {
                $scope.back();
            });
        }
    }

    /** Validação de data e hora **/
    var validateInsertOrUpdate = function () {
        var semana = ["Domingo", "Segunda-Feira", "Terça-Feira", "Quarta-Feira", "Quinta-Feira", "Sexta-Feira", "Sábado"];
        var date = new Date();
        if (semana[date.getDay()] == semana[5] && date.getHours() > 13) {
            return false;
        }
        return true;
    }

    /** Validação do tamanho do campo descrição da atividade **/
    var validateAtividades = function (atividade) {
        if (!atividade) return
        var tipoAtividade = atividade.tipoAtividade;
        if (tipoAtividade.cdTpAtividade === 2 || tipoAtividade.cdTpAtividade === 4) {
            if (atividade.dsAtividade.trim().length < 50) {
                return false;
            }
        }
        return true;
    }

    /** Remoção da Atividade **/
    $scope.removeAtividade = function (atividade) {
        if (atividade.tipoAtividade.cdTpAtividade === 4) {
            $scope.error = "Atividade de manutenção urgente não pode ser removida!";
        } else {
            atividadeService.remove(atividade, function() {
                $scope.back();
            });
        }
    }

    active();
});
