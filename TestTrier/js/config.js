app.config(function ($routeProvider) {
  $routeProvider
  .when('/', {
    templateUrl: 'view/listAtividade.html',
    controller: 'atividadeController',
			editing: false,
			inserting: false
  })
  .when('/cadAtividade', {
    templateUrl: 'view/cadAtividade.html',
    controller: 'atividadeController',
			editing: false,
			inserting: true
  })
  .when('/editAtividade', {
    templateUrl: 'view/cadAtividade.html',
    controller: 'atividadeController',
			editing: true,
			inserting: false
  })
  .otherwise({
    templateUrl: 'view/listAtividade.html',
    controller: 'atividadeController'
  })
});
