app.factory("requestFactory", function($http) {

    var _GET = "GET";
    var _POST = "POST"

    var _request = function (http, rest, method, domain, success, error) {
        var url = "http://localhost:8080/TestTrier/rest/" + rest + "/" + method;
        console.log(url);
        $http({
            method: http,
            url: url,
            headers: {
                'Content-Type': 'application/json'
            },
            data: domain
        }).then(function successCalback(response) {
            if (success) success(response);
        }, function errorCalback(responseError) {
            if (error) error(responseError);
        })
    };


    var findAll = function (service, success) {
        _request(_GET, service, "findAll", undefined, success);
    }

    var findByExample = function (service, filter, success) {
        _request(_POST, service, 'findByExample', filter, success);
    }

    var insert = function (service, filter, success) {
        _request(_POST, service, 'insert', filter, success);
    }

    var update = function (service, filter, success, error) {
        _request(_POST, service, 'update', filter, success, error);
    }

    var remove = function (service, filter, success, error) {
        _request('DELETE', service, undefined, filter, success, error);
    }

    return {
        findAll: findAll,
        findByExample: findByExample,
        insert: insert,
        update: update,
        remove: remove
    }
});
